function paintingListing(base) {
    chiefListing = document.createElement('div');
    chiefListing.className = "listingBig";
    var container = document.getElementById('sorting');
    container.parentNode.insertBefore(chiefListing,container.nextSibling);

    base.forEach(function (base_item, i) {
        var div = document.createElement('div');
        div.className = "listing";
        div.dataset.id_people = base_item.id;

        div.onclick = function (event) {
            hiddenBloc(event.currentTarget);
            popupShowClose('block');
        }
        chiefListing.appendChild(div);

        var image = document.createElement('img');
        image.className = "avatar";
        image.src = base_item.picture.medium;
        div.appendChild(image);

        var div_fio = document.createElement('div');
        div_fio.className = "fio";
        div_fio.innerHTML = base_item.name.title + '.' + ' ' + base_item.name.first + ' ' + base_item.name.last;
        div.appendChild(div_fio);

    });

}

function getPaintingPopup(OneMan) {
    popup_window = document.createElement('div');
    popup_window.className = "popup_window";
    popup_window.id = "box";
    popup_window.onclick = function () {
        popupShowClose('none');
        deleteFunction();
    }
    document.body.appendChild(popup_window);

    var popup = document.createElement('div');
    popup.className = "popup";
    popup.id = "box-two";
    popup_window.appendChild(popup);

    var close = document.createElement('div');
    close.className = "close";
    close.innerHTML = 'X';

    // close.onclick = function () {
    //     popupShowClose('none');
    //     deleteFunction();
    // }
    popup.appendChild(close);

    var avatar_large = document.createElement('img');
    avatar_large.className = "avatar-large";
    avatar_large.src = OneMan.picture.large;
    popup.appendChild(avatar_large);

    var namepopup = document.createElement('div');
    namepopup.className = "namepopup";
    namepopup.innerHTML = 'Это' + ' ' + OneMan.name.first;
    popup.appendChild(namepopup);

    var street = document.createElement('div');
    street.className = "data";
    street.innerHTML = 'Улица' + ': ' + OneMan.location.street;
    popup.appendChild(street);

    var city = document.createElement('div');
    city.className = "data";
    city.innerHTML = 'Город' + ': ' + OneMan.location.city;
    popup.appendChild(city);

    var state = document.createElement('div');
    state.className = "data";
    state.innerHTML = 'Штат' + ': ' + OneMan.location.state;
    popup.appendChild(state);

    var email = document.createElement('div');
    email.className = "data";
    email.innerHTML = 'email' + ': ' + OneMan.email;
    popup.appendChild(email);

    var phone = document.createElement('div');
    phone.className = "data";
    phone.innerHTML = 'Телефон' + ': ' + OneMan.phone;
    popup.appendChild(phone);

}

var popup_window;
function deleteFunction() {
    popup_window.parentNode.removeChild(popup_window);
}

function popupShowClose(state) {
    document.getElementById('box').style.display = state;
    document.getElementById('box-two').style.display = state;

}

function displayError() {
    var display_error = document.createElement('div');
    display_error.className = "error";
    document.body.appendChild( display_error );

    var display_error_text = document.createElement('div');
    display_error_text.className = "error_text";
    display_error_text.innerHTML = 'Данные не найдены.' + '</br>' + ' Видимо произошла какая-то ошибка...';
    display_error.appendChild( display_error_text );

}