window.onload = function() {
  getInformationPeople();
};

arrayPeople = new Array();

function getInformationPeople() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://api.randomuser.me/1.0/?results=50&nat=gb,us&inc=gender,name,location,email,phone,picture', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var base = JSON.parse(xhr.responseText);
            arrayPeopleEmpty = new Array();
            for (var i = 0; i < base.results.length; i++) {
                base.results[i].id = i;
                arrayPeopleEmpty[i] = base.results[i];
            }
            arrayPeople = arrayPeopleEmpty;
            paintingListing(arrayPeople);
            document.getElementById('footer').style.display = 'block';
        } else if(xhr.readyState == 4 && xhr.status != 200){
            displayError();
        }
    }
};

function hiddenBloc(event) {
    var one_people;
    var id_people = event.dataset.id_people;
    for (var i = 0; i < arrayPeople.length; i++) {
        if (arrayPeople[i].id == id_people) {
            one_people = arrayPeople[i];
            break;
        }
    }
    getPaintingPopup(one_people);
}





