function sort() {
    var sort_array_people = arrayPeople.sort(sortingButtom);
    deleteFunctionListing();
    paintingListing( sort_array_people );
}

function sortingButtom( a, b) {
    var a_name = a.name.first + a.name.last;
    var b_name = b.name.first + b.name.last;
    if (a_name > b_name) {
        return 1;
    }
    if (a_name < b_name) {
        return -1;
    }
    return 0;
}

function deleteFunctionListing() {
    chiefListing.parentNode.removeChild( chiefListing ) ;
}

function reversSort() {
    var sort_array_people = arrayPeople.sort( sortingButtom );
    deleteFunctionListing();
    var revers_list = sort_array_people.reverse();
    paintingListing( revers_list );
}

function sortFirstLetter() {
    var sort_array_people = arrayPeople.sort( sortingButtomsortFirstLetter );
    deleteFunctionListing();
    paintingListing( sort_array_people );
}

function sortingButtomsortFirstLetter( a, b) {
    if (a.name.first[0] > b.name.first[0]) {
        return 1;
    }
    if (a.name.first[0] < b.name.first[0]) {
        return -1;
    }
    return 0;
}

function sortFirstLetterRevers() {
    var sort_array_people = arrayPeople.sort( sortingButtomsortFirstLetter );
    deleteFunctionListing();
    var revers_list = sort_array_people.reverse();
    paintingListing( revers_list );
}


